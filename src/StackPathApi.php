<?php

namespace Drupal\stackpath;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Psr\Http\Message\ResponseInterface;

/**
 * StackPath API wrapper.
 *
 * @package Drupal\stackpath
 */
class StackPathApi {

  /**
   * StackPath API URI.
   */
  const STACKPATH_API_URI = "https://gateway.stackpath.com";

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Token.
   *
   * @var string
   */
  protected $token;

  /**
   * Logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * StackPathApi constructor.
   *
   * @param \Drupal\Core\Http\ClientFactory $clientFactory
   *   Client Factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   Logger.
   */
  public function __construct(ClientFactory $clientFactory, LoggerChannelInterface $loggerChannel) {
    $this->httpClient = $clientFactory->fromOptions(
      [
        'base_uri' => self::STACKPATH_API_URI,
        'timeout' => 30,
      ]
    );
    $this->logger = $loggerChannel;
  }

  /**
   * Create hash tag.
   *
   * @param string $cache_tag
   *   Tag.
   *
   * @return bool|string
   *   Hash tag with 5 characters, or FALSE on failure.
   */
  public function createHashTag($cache_tag) {
    return substr(md5(Settings::getHashSalt() . $cache_tag), 0, 5);
  }

  /**
   * Adds default parameters.
   *
   * @param array $input
   *   Additional parameters.
   *
   * @return array
   *   Array of request parameters.
   */
  private function prepareRequestParameters(array $input = []) {
    $parameters = [];
    if (!empty($input)) {
      $parameters['json'] = $input;
    }
    // Default values can be overridden by defining them in the $opts passed to
    // request.
    $default_options = [
      // Unless overridden, every payload will send the Accept header set to
      // application/json.
      'headers' => [
        'Accept' => 'application/json',
      ],
    ];
    // If the bearer token has been retrieved, supply it as the auth header.
    if (isset($this->token)) {
      $default_options['headers']['Authorization'] = sprintf("Bearer %s", $this->token);
    }
    // Recursive merge of custom options over defaults.
    $parameters = array_merge_recursive($default_options, $parameters);
    return $parameters;
  }

  /**
   * Get Token.
   *
   * @param string $client_id
   *   Client ID.
   * @param string $client_secret
   *   Client secret.
   *
   * @return bool|mixed|string
   *   Token string. FALSE in case of failure.
   */
  public function getToken($client_id, $client_secret) {
    if (!empty($this->token)) {
      return $this->token;
    }
    $params = [
      'client_id' => $client_id,
      'client_secret' => $client_secret,
      'grant_type' => 'client_credentials',
    ];
    try {
      $response = $this->httpClient->post('/identity/v1/oauth2/token', $this->prepareRequestParameters($params));
      $data = $this->processResponse($response);
      if (!empty($data['access_token'])) {
        $this->token = $data['access_token'];
        return $this->token;
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Process response.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response object.
   *
   * @return mixed
   *   Decoded response.
   */
  private function processResponse(ResponseInterface $response) {
    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Invalidates CDN caches.
   *
   * @param string $stack_id
   *   Stack ID.
   * @param string $type
   *   Cache type.
   * @param array $invalidations
   *   Array of invalidations.
   *
   * @return mixed
   *   Result of the invalidation request. Array with 'error' and 'code' element
   *   if an error occurred.
   */
  public function invalidate($stack_id, $type = '', array $invalidations = []) {
    $root_url = new Url('<front>', [], ['absolute' => TRUE]);
    $url_parts = parse_url($root_url->toString());
    $url = $url_parts['scheme'] . '://' . $url_parts['host'];
    $params = ['items' => []];
    switch ($type) {
      case 'tags':
        // Collect all tags.
        foreach ($invalidations as $invalidation) {
          // Create purge selector to have 1 tag per item.
          // This is weird, but multiple tags in 1 purge selector doesn't work
          // and multiple purge selectors with different tags either, so the
          // only way is to add multiple items that have 1 tag each and 1 purge
          // selector.
          $purgeSelector = [
            'selectorType' => 'TAG',
            'selectorName' => 'X-Tag',
            'selectorValue' => $this->createHashTag($invalidation),
          ];
          $params['items'][] = [
            'url' => $url,
            'purgeSelector' => [$purgeSelector],
            'recursive' => TRUE,
            'invalidateOnly' => FALSE,
          ];
        }
        break;

      case 'url':
        foreach ($invalidations as $invalidation) {
          $params['items'][] = [
            'url' => $invalidation,
            'recursive' => FALSE,
          ];
        }
        break;

      default:
        $params['items'][] = [
          'url' => $url,
          'recursive' => TRUE,
        ];
        break;
    }
    try {
      $params = $this->prepareRequestParameters($params);
      $response = $this->httpClient->post('/cdn/v1/stacks/' . $stack_id . '/purge', $params);
      return $this->processResponse($response);
    }
    catch (\Exception $e) {
      return [
        'error' => $e->getMessage(),
        'code' => $e->getCode(),
      ];
    }
  }

}
