<?php

namespace Drupal\stackpath\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;

/**
 * Class StackPathPurgerItemsAlter.
 *
 * Contains the event for altering items before sending
 * the request for purging.
 */
class StackPathPurgerItemsAlter extends Event {

  /**
   * The purger instance.
   *
   * @var \Drupal\purge\Plugin\Purge\Purger\PurgerInterface
   */
  protected $purger;

  /**
   * The list of items to invalidate (tags or urls).
   *
   * @var array
   */
  protected $items = [];

  /**
   * Constructs StackPathPurgerItemsAlter.
   *
   * @param \Drupal\purge\Plugin\Purge\Purger\PurgerInterface $purger
   *   The purger.
   * @param array $items
   *   The items.
   */
  public function __construct(PurgerInterface $purger, array $items = []) {
    $this->purger = $purger;
    $this->items = $items;
  }

  /**
   * Gets the items to alter.
   *
   * @return array
   *   The list of items to alter (tags or urls).
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Sets the items after altering.
   *
   * @param array $items
   *   The items.
   *
   * @return \Drupal\stackpath\Event\StackPathPurgerItemsAlter
   *   The event object.
   */
  public function setItems(array $items) {
    $this->items = $items;
    return $this;
  }

  /**
   * Gets purger.
   *
   * @return \Drupal\purge\Plugin\Purge\Purger\PurgerInterface
   *   The purger that is used to invalidate the items.
   */
  public function getPurger() {
    return $this->purger;
  }

}
