<?php

namespace Drupal\stackpath\Event;

/**
 * Class StackPathPurgerEvents.
 *
 * Contains descriptions of events for StackPath purger.
 */
final class StackPathPurgerEvents {

  /**
   * Event for altering the tags before sending for purging.
   */
  const STACK_PATH_TAGS_ALTER = 'stackpatch.tags_alter';

  /**
   * Event for altering the urls before sending for purging.
   */
  const STACK_PATH_URLS_ALTER = 'stackpatch.urls_alter';

}
