<?php

namespace Drupal\stackpath\Plugin\Purge\Purger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;
use Drupal\stackpath\Entity\StackPathPurgerSettings;
use Drupal\stackpath\Event\StackPathPurgerEvents;
use Drupal\stackpath\Event\StackPathPurgerItemsAlter;
use Drupal\stackpath\StackPathApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * StackPath Purger implementation.
 *
 * @PurgePurger(
 *   id = "purge_purger_stackpath",
 *   label = @Translation("StackPath Purger"),
 *   configform = "\Drupal\stackpath\Form\StackPathPurgerConfigForm",
 *   cooldown_time = 1.0,
 *   description = @Translation("Invalidates the StackPath cache."),
 *   multi_instance = TRUE,
 *   types = {"tag", "url", "everything"},
 * )
 */
class StackPathPurger extends PurgerBase implements PurgerInterface {

  /**
   * StackPath API.
   *
   * @var \Drupal\stackpath\StackPathApi
   */
  protected $stackPath;

  /**
   * Configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $factory;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The settings entity holding all configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Constructs an instance of StackPathPurger.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\stackpath\StackPathApi $stackpath_api
   *   StackPath API service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $factory
   *   Configuration factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StackPathApi $stackpath_api, ConfigFactoryInterface $factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->settings = $factory->get('stackpath.settings.' . $this->getId());
    $this->factory = $factory;
    $this->stackPath = $stackpath_api;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('stackpath.api'),
      $container->get('config.factory'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    if ($this->settings->get('name')) {
      return $this->settings->get('name');
    }
    else {
      return parent::getLabel();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    throw new \LogicException('You should not be here.');
  }

  /**
   * {@inheritdoc}
   */
  public function routeTypeToMethod($type) {
    $methods = [
      'tag'  => 'invalidateTags',
      'url'  => 'invalidateUrls',
      'everything' => 'invalidateAll',
    ];
    return $methods[$type] ?? 'invalidate';
  }

  /**
   * Invalidate a set of urls.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateUrls(array $invalidations) {
    $urls = [];
    // Set all invalidation states to PROCESSING before kick off purging.
    /** @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      try {
        $url = $invalidation->getUrl()->getInternalPath();
      }
      catch (\UnexpectedValueException $e) {
        $url = $this->normalizePath($invalidation->getUrl()->getUri());
      }
      $urls[] = $url;
    }

    // Allow others to alter the list of urls to purge.
    $event = new StackPathPurgerItemsAlter($this, $urls);
    $this->eventDispatcher->dispatch($event, StackPathPurgerEvents::STACK_PATH_URLS_ALTER);
    $urls = $event->getItems();

    if (!empty($urls)) {
      // Invalidate and update the item state.
      $invalidation_state = $this->invalidateItems('urls', $urls);
    }
    else {
      // If no urls were extracted from invalidations, then there is no need
      // to process them again. Mark as success and clear from the queue.
      $invalidation_state = InvalidationInterface::SUCCEEDED;
    }

    $this->updateState($invalidations, $invalidation_state);
  }

  /**
   * Invalidate a set of tags.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateTags(array $invalidations) {
    $tags = [];
    // Set all invalidation states to PROCESSING before kick off purging.
    /** @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      $tags[] = $invalidation->getExpression();
    }

    // Allow others to alter the list of tags to purge.
    $event = new StackPathPurgerItemsAlter($this, $tags);
    $this->eventDispatcher->dispatch($event, StackPathPurgerEvents::STACK_PATH_TAGS_ALTER);
    $tags = $event->getItems();

    if (!empty($tags)) {
      $invalidation_state = $this->invalidateItems('tags', $tags);
    }
    else {
      // If no tags were extracted from invalidations, then there is no need
      // to process them again. Mark as success and clear from the queue.
      $invalidation_state = InvalidationInterface::SUCCEEDED;
    }
    $this->updateState($invalidations, $invalidation_state);
  }

  /**
   * Invalidate everything.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Drupal\purge\Plugin\Purge\Invalidation\Exception\InvalidStateException
   */
  public function invalidateAll(array $invalidations) {
    $this->updateState($invalidations, InvalidationInterface::PROCESSING);
    // Invalidate and update the item state.
    $invalidation_state = $this->invalidateItems();
    $this->updateState($invalidations, $invalidation_state);
  }

  /**
   * Invalidate StackPath CDN cache.
   *
   * @param mixed $type
   *   Type to purge like tags/url. If null, will purge everything.
   * @param string[] $invalidates
   *   A list of items to invalidate.
   *
   * @return int
   *   Returns invalidate items.
   */
  protected function invalidateItems($type = NULL, array $invalidates = []) {
    try {
      $this->stackPath->getToken($this->settings->get('client_id'), $this->settings->get('client_secret'));
      // Purge everything for the given zone.
      /** @var \Psr\Http\Message\ResponseInterface $response */
      $response = $this->stackPath->invalidate($this->settings->get('stack_id'), $type, $invalidates);

      // If successfully clears cache.
      if (empty($response['error'])) {
        $this->logger()->debug('StackPath purge successful. Type: %type, Params: %params',
          ['%type' => $type, '%params' => print_r($invalidates, TRUE)]);
        return InvalidationInterface::SUCCEEDED;
      }
      else {
        $this->logger()
          ->error('StackPath purge failed. Status code %code: %message',
            [
              '%code' => $response['code'],
              '%message' => $response['error'],
            ]);
        return InvalidationInterface::FAILED;
      }
    }
    catch (\Exception $e) {
      // If something bad happens.
      $this->logger()->error('StackPath purge request failed. Status code %code: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()]);
      return InvalidationInterface::FAILED;
    }
  }

  /**
   * Converts any path or URL into a normalized path.
   *
   * @param string $url
   *   URL to normalize.
   *
   * @return string
   *   Returns normalized path.
   */
  public function normalizePath($url) {
    $parsed_url = parse_url($url);
    $path = $parsed_url['path'] ?? '';
    $query = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
    return $path . $query;
  }

  /**
   * Update the invalidation state of items.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   * @param int $invalidation_state
   *   The invalidation state.
   *
   * @throws \Drupal\purge\Plugin\Purge\Invalidation\Exception\InvalidStateException
   */
  protected function updateState(array $invalidations, $invalidation_state) {
    // Update the state.
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($invalidation_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    StackPathPurgerSettings::load($this->getId())->delete();
  }

}
