<?php

namespace Drupal\stackpath\Plugin\Purge\TagsHeader;

use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderBase;
use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderInterface;
use Drupal\stackpath\StackPathApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add X-Tag to the response headers.
 *
 * @PurgeTagsHeader(
 *   id = "stack_path",
 *   header_name = "X-Tag",
 * )
 */
class StackPathTagsHeader extends TagsHeaderBase implements TagsHeaderInterface {

  /**
   * StackPath API service.
   *
   * @var \Drupal\stackpath\StackPathApi
   */
  protected $stackPath;

  /**
   * StackPathTagsHeader constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\stackpath\StackPathApi $stackpath_api
   *   StackPath API service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StackPathApi $stackpath_api) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->stackPath = $stackpath_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('stackpath.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(array $tags) {
    $hashes = [];
    foreach ($tags as $tag) {
      $hashes[] = $this->stackPath->createHashTag($tag);
    }
    return implode(',', $hashes);
  }

}
