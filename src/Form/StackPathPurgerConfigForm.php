<?php

namespace Drupal\stackpath\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;
use Drupal\stackpath\Entity\StackPathPurgerSettings;
use Drupal\stackpath\StackPathApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for Stackpath configurable purgers.
 */
class StackPathPurgerConfigForm extends PurgerConfigFormBase {

  /**
   * The http client.
   *
   * @var \Drupal\stackpath\StackPathApi
   */
  protected $stackPath;

  /**
   * Constructs a \Drupal\purge_purger_http\Form\ConfigurationForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\stackpath\StackPathApi $stackpath_api
   *   StackPath API service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StackPathApi $stackpath_api) {
    parent::__construct($config_factory);
    $this->stackPath = $stackpath_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('stackpath.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'stackpath.config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = StackPathPurgerSettings::load($this->getId($form_state));
    $form['name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#description' => $this->t('A label that describes this purger.'),
      '#default_value' => $settings->name,
      '#required' => TRUE,
    ];
    $form['stack_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Stack ID'),
      '#required' => TRUE,
      '#description' => $this->t('Enter the stack id here.'),
      '#default_value' => $settings->stack_id,
    ];
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#required' => TRUE,
      '#description' => $this->t('Enter the client id here.'),
      '#default_value' => $settings->client_id,
    ];
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#required' => TRUE,
      '#description' => $this->t('Enter the client secret here.'),
      '#default_value' => $settings->client_secret,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $client_id = $form_state->getValue('client_id');
    $client_secret = $form_state->getValue('client_secret');

    // If client secret and client id are set.
    if (!empty($client_id) && !empty($client_secret)) {
      try {
        if (!$this->stackPath->getToken($client_id, $client_secret)) {
          $form_state->setErrorByName('client_id', $this->t('Credential are invalid, please try again with different ID/Secret.'));
        }
      }
      catch (\Exception $e) {
        $form_state->setErrorByName('client_id', $this->t('It looks like StackPath is not responding or something is wrong.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $settings = StackPathPurgerSettings::load($this->getId($form_state));

    // Iterate the config object and overwrite values found in the form state.
    foreach ($settings as $key => $default_value) {
      if (!is_null($value = $form_state->getValue($key))) {
        $settings->$key = $value;
      }
    }
    $settings->save();
  }

}
