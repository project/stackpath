<?php

namespace Drupal\stackpath\Entity;

use Drupal\purge\Plugin\Purge\Purger\PurgerSettingsBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerSettingsInterface;

/**
 * Defines the Stackpath purger settings entity.
 *
 * @ConfigEntityType(
 *   id = "stack_path_purger",
 *   label = @Translation("Stackpath purger settings"),
 *   config_prefix = "settings",
 *   static_cache = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "stack_id",
 *     "client_id",
 *     "client_secret",
 *     "uuid",
 *   },
 * )
 */
class StackPathPurgerSettings extends PurgerSettingsBase implements PurgerSettingsInterface {

  /**
   * The readable name of this purger.
   *
   * @var string
   */
  public $name = '';

  /**
   * The stack ID.
   *
   * @var string
   */
  // @codingStandardsIgnoreLine
  public $stack_id;

  /**
   * The client ID.
   *
   * @var string
   */
  // @codingStandardsIgnoreLine
  public $client_id;

  /**
   * The client secret.
   *
   * @var string
   */
  // @codingStandardsIgnoreLine
  public $client_secret;

}
