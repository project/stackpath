Introduction
=======================

This module:  
    1. Sets the X-Tag http header required by StackPath for tag based Purging
       in Drupal 8. No config is required for this.
    2. Offers a purger plugin for invalidating via tag at StackPath. See below.  
    3. Recommended modules:  
       - purge_queuer_coretags (provides the queueing of the invalidations made
         by Drupal core)
       - purge_processor_cron (actual processor of the queue)   
       - purge_drush (all above in one but with drush)  

To enable purging, browse to admin/config/development/performance/purge and
add the purger provided by this module.  To configure the purger, you need a
StackPath stack id, client id and secret for authentication.  Visit
https://www.stackpath.com/ to get that info.

Maintainers
=================
- Artem Dmitriiev (admitriiev) https://www.drupal.org/u/admitriiev
- Christoph Breidert (breidert) https://www.drupal.org/u/breidert
